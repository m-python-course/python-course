Easygui
=======

Easygui - библиотека, которая используется для быстрого создания простых
графичеческих интерфейсов

Функции
-------

.. function :: msgbox(message)

    Создаёт новое окно, в котором будет написано сообщение ``message`` и
    кнопка **ok**

.. function :: buttonbox(message, choices=[])

    Создаёт новое окно с заданными кнопками. В возвращает текст кнопки,
    которая была нажата

.. function :: integerbox(message, lowerbound=0, upperbound=99)

    Создаёт новое окно, в котором можно вводить числа в диапозоне от
    ``lowerbound`` до ``upperbound``, возвращает введённое пользователем число


Официальная документация: https://easygui.readthedocs.io/en/master/api.html


Задание
-------

Используя шаблон проекта, реализуйте игры «Камень, ножницы, бумага» и
«Угадай число»::

    import easygui


    def rock_paper_scissors():
        easygui.msgbox('Здесь будет игра "Камень, ножницы, бумага"')


    def guess_the_number():
        easygui.msgbox('Здесь будет игра "Угадай число"')


    games = [
        'Камень, ножницы, бумага',
        'Угадай число'
    ]

    games_entry_points = [
        rock_paper_scissors,
        guess_the_number
    ]

    while True:
        res = easygui.buttonbox('Выбери игру!', choices=games)
        if res is None:
            break
        games_entry_points[games.index(res)]()

Шаблон проекта в архиве: `games.zip <../_static/lesson13/games.zip>`_


Инструкция по установке
-----------------------

1. Запустить программу "командная строка" (cmd.exe)
2. Ввести команду "python -m pip install easygui"

Где:

``python`` - интерпретатор питона;
``-m`` - команда на запуск другой программы;
``pip`` - менеджер пакетов;
``install`` - команда на установку какой-либо библиотеки;
``easygui`` - название библиотеки.
