Словари
=======

Основные определения
--------------------

Словарь
    Неупорядоченная коллекция объектов произвольного типа с доступом по ключу.
    Представлены типом ``dict``.

Множество
    Неупорядоченная коллекция объектов произвольного типа, в которой
    не имеются дубликаты. Представлены типом ``set``.

Неупорядоченная коллекция
    Коллекция, в которой элементы не индексируются и нельзя делать срезы


Создание словарей
-----------------

  >>> d = {}
  >>> type(d)
  <class 'dict'>

  >>> d = {"Name": "John", "Last Name": "Connor"}
  >>> d
  {'Name': 'John', 'Last Name': 'Connor'}


Динамическое создание словарей::

  # Словарь можно создать, передав список пар ключ-значение
  pairs = [('Name', 'John'), ('Age', 19)]
  d = dict(pairs)
  print(d)
  {'Name': 'John', 'Age': 19}


  # Можно создавать такие пары с помощью функции zip
  id = (1, 2, 3, 4,)
  logins = ('Vasya', 'Petya', 'Lera')
  # Функция zip упаковывает несколько последовательностей в кортежи
  d = dict(zip(logins, id))
  print(d)
  {'Vasya': 1, 'Petya': 2, 'Lera': 3}


Добавление элементов осуществляется с помощью фигурных скобок::

  >>> d['Age'] = 19

Получение элемента по ключу::

  >>> d['Age']
  19

Методы словарей
---------------

.. function :: d.get(key)

   Получение элемента по заданному ключу::

     >>> d.get('Name')
     'John'
     >>> d.get('Not Presented Key')
     None

.. function :: d.keys()

   Получение списка всех ключей

     >>> d.keys()
     dict_keys(['Name', 'Last Name'])

.. function :: d.values()

   Получение списка всех значений в словаре

     >>> d.values()
     dict_values(['John', 'Connor'])

.. function :: d.items()

   Получение списка пар ключ-значение, представленных в виде кортежей::

     >>> d.items()
     dict_items([('Name', 'John'), ('Last Name', 'Connor')])

.. function :: d.pop(key)

   Извлечение элемента из словаря::

     >>> d = {'Name': 'John', 'Last Name': 'Connor'}
     >>> d.pop('Name')
     'John'
     >>> d
     {'Last Name': 'Connor'}

.. function :: dict.fromkeys(seq, value)

   Создаёт новый словарь, в котором ключи взяты из последовательности ``seq``
   и им установлены значения по умолчанию ``value``::

     >>> lst = ['uesr1', 'user2', 'user3']
     >>> user_logins_with_passwords = dict.fromkeys(lst, 123456)
     >>> user_logins_with_passwords
     {'user1': 123456, 'user2': 123456, 'user3': 123456}


Задачи
------

1. Напишите программу, которая получает на вход набор слов и выводит на экран
   только уникальные. Использовать тип ``set``.

2. Доработать программу, чтобы она получала от пользователя путь до файла
   со словами

3. Написать программу, которая на вход получает файл и выводит на экран какое
   слово сколько раз встречалось. Использовать тип ``dict``. Содержимое файла::

    слово
    фраза
    предложение
    слово

   Пример вывода::

    Путь к файлу: words.txt
    слово: 2
    фраза: 1
    предложение: 1


4. Создайте функцию ``compute_bill``, считающаю итоговую сумму товаров в чеке.
   Функция должна принимать 1 параметр - словарь, в котором указано количество
   едениц товара. Цены хранятся в словаре::

     prices = {
       "banana": 4,
       "apple": 2,
       "orange": 1.5,
       "pear": 3
     }

5. Создайте программу, которая будет выводить все возможные комбинации при
   броске 2 игральных костей и сумму их значений. Пример вывода::

     2: [(1, 1)]
     3: [(1, 2), (2, 1)]
     ...
