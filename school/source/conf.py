import os
import sys

file_path = os.path.dirname(os.path.abspath(__file__))
conf_path = os.path.join(file_path, '..', '..', 'index', 'source')
sys.path.append(conf_path)

from conf import *
