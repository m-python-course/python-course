Обработка столкновений
======================

1. Реализуйте игровую сцену, состоящую из движущейся цели и пушки, стреляющей
   снарядами.

.. literalinclude :: ../_static/lesson22/00_cannon.py

2. Реализуйте простейший тир, в котором игрок должен кликать по цели.

.. literalinclude :: ../_static/lesson22/01_click_the_circle.py

3. Реализуйте движущийся шар, отскакивающий от стен.

.. literalinclude :: ../_static/lesson22/02_ball.py

Звук выстрела: `shot.wav <../_static/lesson22/shot.wav>`_.
