Основы работы с библиотекой requests
====================================

В основе ежедневной работы пользователей в сети Интернет лежит обмен данными
между клиентскими приложениями (например браузер) и веб-серверами. В
соответствии с прикладным протоколом HTTP, клиент отправляет запрос (request)
на получение определенного ресурса к серверу и получает от него ответ
(response). Ответ, помимо самого запрошенного ресурса, содержит также служебную
информацию.

Библиотека ``requests`` предоставляет удобный способ отправки HTTP-запросов и
обработки ответов от сервера.

Рассмотрим функцию ``get``, которая нам потребуются для отправки простых
запросов.

.. function :: requests.get(url, params=None, **kwargs)

  Отправить GET-запрос. Функция принимает URL-адрес запрашиваемого ресурса
  и возвращает объект типа ``Response``. Также возможна передача в функцию
  дополнительных параметров запроса.

  Пример отправки запроса на получение ресурса::

    url = 'http://api.forismatic.com/api/1.0/'
    payload  = {'method': 'getQuote', 'format': 'json', 'lang': 'ru'}
    res = requests.get(url, params=payload))

  Теперь мы можем получить все необходимые данные из Response-объекта res.

  Проверяем URL ресурса::

    >>> res.url
    'http://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=ru'

  Проверяем код ответа HTTP::

    >>> res.status_code
    200

  Получаем заголовки ответа::

    >>> res.headers
    {'Date': 'Tue, 19 Feb 2019 19:27:34 GMT', 'Content-Type': 'application/json' ... }

  Получаем содержимое ответа в текстовом формате::

    >>> res.text

  В случае бинарного содержимого ответа воспользуемся атрибутом ``content``::

    >>> res.content

  Если данные в ответе представлены в формате json их можно декодировать
  и получить словарь::

    >>> data = res.json()
    >>> data['quoteText']
    'Тот, кто не смотрит вперед, оказывается позади'
    >>> data['quoteAuthor']
    'Герберт Уэллс '


Подробная документация к библиотеке доступна по адресу: http://docs.python-requests.org/en/master/api/

Множество интернет-сервисов предоставляют программистам API (Application
programming interface) — интерфейс получения данных посредством запросов, а
также документацию к нему.


Задачи
------

1. Реализовать приложение для получения случайных цитат.

   Шаг 1. Изучите API сервиса forismatic.com: http://forismatic.com/en/api/

   Шаг 2. С помощью модуля ``easygui`` разработайте приложение с графическим
   интерфейсом для получения цитат на русском и английском языках.


2. Реализовать приложения для загрузки картинок с котиками:

   Шаг 1. Изучите API сервиса cataas.com: https://cataas.com/#/

   Шаг 2. С помощью модуля ``easygui`` разработайте приложение для загрузки
   случайных картинок с котиками. Картинка содержится в ответе от сервера в
   бинарном виде.
   
3. Реализовать приложение, которое запрашивает у пользователя дату и возвращает список праздников в эту дату:

   Шаг 1. Изучите API сервиса: https://date.nager.at/

   Шаг 2. С помощью модуля ``easygui`` разработайте приложение, которое принимает на вход дату в определённом формате, а 
   возвращает список праздников в эту дату.

