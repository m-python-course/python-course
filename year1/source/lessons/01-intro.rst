Введение. Объекты. Переменные. Основы ввода и вывода данных.
============================================================

`Слайды <../_static/lesson1/index.html>`_


Основные определения
--------------------

Интерпретатор
    Программа, реализующая построчный анализ, обработку и выполнение исходного
    кода программы.

IDLE (читается — *айдл*)
    (Integrated Development and Learning Environment) — это интегрированная
    среда разработки и обучения на языке Python.

Данные в языке Python представлены в форме **объектов**.

Каждый объект имеет **идентификатор**, **тип** и **значение**.

Литерал
    Форма записи для «фиксированного» представления значения объекта. Примеры
    литералов: ``42``, ``4.2``, ``"Hello, World!"``.

Идентификатор
    Однозначно идентифицирует объект. Его можно считать адресом области в памяти
    компьютера, где хранится объект.

Тип данных
    Тип определяет возможные значения и их смысл, операции, а также способы
    хранения значений типа.

Переменная
    Именованная ссылка на конкретный объект.

**Оператор присваивания** связывает переменную и объект. Пример::

    >>> x = 5

Первую привязку переменной к объекту в программе называем **определением** и
**инициализацией** переменной.


Изученные средства языка
------------------------

.. function:: print(a)
              print(a, b, c)

   Функция принимает на вход объекты, преобразует их в строки и выводит в 
   стандартный поток вывода.

   Функция ``print`` может принимать дополнительные параметры, с ними мы
   познакомимся далее в курсе.

   Примеры использования::
     
     # Функция может принимать литералы…
     print(42)
     print("answer")

     # … объекты
     answer = 42
     print(answer)

     # …строки формата
     print(f"The answer is {answer}")

     import math.pi

     # Вывод числа π с 4 знаками после запятой
     print(f"Pi = {math.pi:.4f}")

.. function:: input(prompt)

   Выводит в стандартный поток вывода приглашение ``prompt``, а затем считывает
   введенные пользователем данные и возвращает строку.

   Примеры использования в IDLE::

     >>> name = input("What is your name? ")
     What is your name? James
     >>> name
     'James'

   Пример ввода целых чисел::
     
     a = int(input("a = "))
     b = int(input("b = "))
     s = a + b
     print(f"{a} + {b} = {s}")

.. function:: type(object)

   Возвращает тип объекта ``object``.

   Примеры использования::

     >>> type(42)
     <class 'int'>
     >>> type(42.0)
     <class 'float'>
     >>> type("Python")
     <class 'str'>

.. function:: int(x)

   Принимает число или строку и возвращает целое число со знаком.

   Примеры использования::

     >>> int("42")
     42
     >>> int(42.5)
     42

.. function:: float(x)

   Принимает число или строку и возвращает число с плавающей точкой.

   Примеры использования::

     >>> float("42")
     42.0
     >>> float("42.5")
     42.5
     >>> float(42)
     42.0


О важности имен переменных
--------------------------

Что делает этот код?

.. literalinclude:: ../_static/py/week01/distance_bad_names.py

Ответить на вопрос затруднительно. Можно рассказать о том, что происходит в
каждой строчке: какие функции вызываются, какие операции выполняются, но смысл
кода может оставаться неясным. Приложение ожидает на вход четыре каких-то
числа, выводит какое-то новое число. Понять смысл такого кода может оказаться
непросто из-за неудачного выбора имен переменных: ``a``, ``b``, ``c``, ``d``,
``e``. Такие имена легко печатать, но сложно помнить смысл, спрятанный за ними.

Переименуем наши переменные и изменим приглашения для ввода:

.. literalinclude:: ../_static/py/week01/distance_good_names.py

Смысл кода сразу же становится понятен: приложение ожидает на вход координаты
двух точек и вычисляет расстояние между ними. Можно не помнить формулы или не
понимать каждой строки кода — удачно выбранные имена переменных помогут
сориентироваться.


Задачи
------

1. Реализовать приложение для вычисления периметра и площади прямоугольника.
   Приложение запрашивает у пользователя два целых числа — длины сторон
   прямоугольника. Пример работы приложения::

     Input a: 5
     Input b: 4

     P = 18
     S = 20

2. Напишите программу, которая считывает значения двух переменных ``a`` и ``b``,
   затем меняет их значения местами (то есть в переменной ``a`` должно быть
   записано то, что раньше хранилось в ``b``, а в переменной ``b`` записано то,
   что раньше хранилось в ``a``). Затем выведите значения переменных.

3. Напишите программу, которая приветствует пользователя, выводя слово Hello,
   введенное имя и знаки препинания по образцу. Программа должна считывать в
   строковую переменную значение и писать соответствующее приветствие.

   Пример::

     Your name: Harry
     Hello, Harry!

4. Даны два целых числа: ``a`` и ``b``. Найти их среднее арифметическое по
   формуле:

   .. math::

     \frac{a + b}{2}

5. Электронные часы показывают время в формате часы(h):минуты(m):секунды(s),
   найти количество секунд прошедшее с начала суток.


Дополнительные задачи
---------------------

1. Напишите программу, которая по данному числу ``n`` от 1 до 9 выводит на экран
   ``n`` пингвинов. Изображение одного пингвина имеет размер 5×9 символов, между
   двумя соседними пингвинами также имеется пустой (из пробелов) столбец.
   Для упрощения рисования скопируйте пингвина из примера в среду разработки.

   Пример::

     Penguins: 3

        _~_       _~_       _~_
       (o o)     (o o)     (o o)
      /  V  \   /  V  \   /  V  \
     /(  _  )\ /(  _  )\ /(  _  )\
       ^^ ^^     ^^ ^^     ^^ ^^

2. Неспокойно сейчас на стапелях шестого дока межгалактического порта планеты
   Торна. Всего через месяц закончится реконструкция малого броненесущего
   корвета «Эния». И снова этому боевому кораблю и его доблестной команде
   предстоят тяжелые бои за контроль над плутониевыми рудниками Сибелиуса.
   Работа не прекращается ни на секунду, лазерные сварочные аппараты работают
   круглые сутки. От непрерывной работы плавятся шарниры роботов-ремонтников. Но
   задержаться нельзя ни на секунду. И вот в этой суматохе обнаруживается, что
   термозащитные панели корвета вновь требуют срочной обработки сульфидом тория.
   Известно, что на обработку одного квадратного метра панели требуется 1
   нанограмм сульфида. Всего необходимо обработать ``N`` прямоугольных панелей
   размером ``A`` на ``B`` метров. Вам необходимо как можно скорее подсчитать,
   сколько всего сульфида необходимо на обработку всех панелей «Энии». И не
   забудьте, что панели требуют обработки с обеих сторон.

3. Решите задачу 2 для целых чисел без использования дополнительной переменной.


Домашнее задание
----------------

1. Установить Python на домашний ПК. Официальный сайт: https://www.python.org/

2. Программа считывает 2 целых числа. Вычислите их сумму, разность,
   произведение, частное.

3. Дана длина ребра куба ``a``. Найти объем куба ``V`` и площадь его поверхности
   :math:`S = 6 \cdot a^2`.
