Циклы
=====


Цикл ``while``
--------------

Цикл ``while`` позволяет многократно выполнять блок команд до тех пор, пока
выполняется некоторое условие. Пример::

  n = 5

  while n > 0:
      print(f'n = {n}')
      n -= 1


Вывод::

  n = 5
  n = 4
  n = 3
  n = 2
  n = 1


Цикл ``for``
------------

Цикл ``for..in`` осуществляет *итерацию* по последовательности объектов, т.е.
проходит через каждый элемент в последовательности. Мы узнаем больше о
последовательностях на следующих уроках, а пока просто запомните, что
последовательность – это упорядоченный набор элементов.

Пример::

  for i in range(1, 6):
      print(f'i = {i}')

Вывод::

  i = 1
  i = 2
  i = 3
  i = 4
  i = 5

В этом примере функция ``range`` возвращает объект последовательнсоти. Обратите
внимание, что правая граница не включается.

Есть несколько способов создания последовательности с помощью функции ``range``.

.. function :: range(stop)
               range(start, stop)
               range(start, stop, step)

   Возвращает последовательность от ``start`` до ``stop`` (не включительно) с шагом
   ``step``.


Оператор ``break``
------------------

Оператор ``break`` служит для прерывания цикла, т.е. остановки выполнения
команд даже если условие выполнения цикла ещё не приняло значения ``False``
или последовательность элементов не закончилась.

Пример::

  while True:
      command = input('> ')
      if command == 'exit':
          break


Оператор ``continue``
---------------------

Оператор ``continue`` используется, чтобы пропустить все оставшиеся команды
в текущем блоке цикла и продолжить со следующей итерации цикла::

Пример::

  for i in range(5):
      if i == 2 or i == 3:
          continue
      print(i)


Задачи
------

В задачах указано, какой цикл рекомендуется использовать для их решения.

1. [for] Пользователь вводит числа ``a`` и ``b``. Вывести все целые числа,
   расположенные между ними.

2. [for] Доработать предыдущую задачу так, чтобы выводились только числа,
   делящиеся на 5 без остатка.

3. [for] Пользователь вводит число. Вывести таблицу умножения на это число

4. [for] Известны оценки абитуриента на четырех экзаменах. Определить сумму
   набранных им баллов. Оценки вводятся в цикле.

5. [while] В бесконечном цикле приложение запрашивает у пользователя числа.
   Ввод завершается, как только пользователь вводит слово ‘end’. После
   завершения ввода приложение выводит сумму чисел.

6. [while] Дано натуральное число. Определить сумму цифр в нем.

7. Пользователь вводит число ``n`` и цифру ``a``. Определить, сколько раз
   цифра встречается в числе. (не использовать метод ``count``)
