import math


point1_x = int(input('Point 1 x: '))
point1_y = int(input('Point 1 y: '))

point2_x = int(input('Point 2 x: '))
point2_y = int(input('Point 2 y: '))

distance = math.sqrt((point2_x - point1_x) ** 2 + (point2_y - point1_y) ** 2)

print(f"Distance: {distance:.4f}")
