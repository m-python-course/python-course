import pygame
import random
from pygame import gfxdraw


class GameState:
    progress = 1
    win = 2
    lose = 3


class Difficulty:
    def __init__(self, width, height, mine_count):
        self.width = width
        self.height = height
        self.mine_count = mine_count


EASY = Difficulty(10, 8, 10)
NORMAL = Difficulty(18, 14, 40)
HARD = Difficulty(24, 20, 99)


class Ui:
    menu_height = 60
    cell_side = 30

    menu_color = (74, 117, 44)

    # Первый цвет в паре - светлый, второй - темный
    closed_colors = ((170, 215, 81), (162, 209, 73))
    open_colors = ((229, 194, 159), (215, 184, 153))
    selected_colors = ((191, 225, 125), (185, 221, 119))

    # Цвет открытой ячейки с миной
    mined_cell_color = (219, 50, 54)
    mine_radius = 8
    mine_color = (0, 0, 0, 200)

    # Цвета счетчиков мин
    mine_count_colors = (
        # Синий
        (25, 118, 210),
        # Зеленый
        (56, 142, 60),
        # Красный
        (211, 47, 47),
        # Сиреневый
        (123, 31, 162)
    )

    flag_color = (230, 51, 7)
    cross_line_width = 3

    menu_font_color = (246, 248, 244)

    def __init__(self):
        self.screen = None
        self.screen_width = 0
        self.screen_height = 0
        self.field = None
        self.difficulty = None
        self.mine_img = self.create_mine_img()
        self.menu_font = pygame.font.SysFont('consolas', 25)
        self.game_state = GameState.progress
        # Список пар (cell_row, cell_col).
        # Ячейки, которые нужно подсветить при мультиклике.
        self.highlighted_cells = []

    def create_mine_img(self):
        mine_img = pygame.Surface((self.cell_side, self.cell_side), pygame.SRCALPHA)
        pygame.gfxdraw.aacircle(
            mine_img, self.cell_side // 2, self.cell_side // 2,
            self.mine_radius, self.mine_color)
        pygame.gfxdraw.filled_circle(
            mine_img, self.cell_side // 2, self.cell_side // 2,
            self.mine_radius, self.mine_color)
        return mine_img

    def start_level(self, difficulty):
        self.difficulty = difficulty
        self.field = Field(
            difficulty.width, difficulty.height, difficulty.mine_count)
        self.game_state = GameState.progress

        self.screen_width = self.cell_side * difficulty.width
        self.screen_height = (self.cell_side * difficulty.height
                              + self.menu_height)
        self.screen = pygame.display.set_mode(
            (self.screen_width, self.screen_height))

    def handle_events(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
                if event.type == pygame.KEYDOWN:
                    # TODO(8): при нажатии на клавиши 1, 2 или 3
                    #  запускать уровень на уровне сложности
                    #  EASY, NORMAL или HARD соответственно.
                    if event.key == pygame.K_r:
                        self.start_level(self.difficulty)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.click_field(event)
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.highlighted_cells = []

            self.draw()

            pygame.display.flip()
            pygame.time.wait(33)

    def click_field(self, event):
        if not self.cursor_in_field(*event.pos):
            return

        if self.game_state != GameState.progress:
            return

        left, _, right = pygame.mouse.get_pressed()
        cursor_x, cursor_y = pygame.mouse.get_pos()
        cell_row, cell_col = self.get_cell_by_pos(cursor_x, cursor_y)

        if left and right:
            self.field.open_near(cell_row, cell_col)
            self.highlighted_cells = self.field.get_near_cells_coords(
                cell_row, cell_col)
            self.field.get_near_cells_coords(cell_row, cell_col)
        elif left:
            self.field.open_cell(cell_row, cell_col)
        elif right:
            self.field.toggle_flag(cell_row, cell_col)

        self.game_state = self.get_game_state()

        if self.game_state == GameState.win:
            self.flag_mines()
        elif self.game_state == GameState.lose:
            self.open_mines()

    def get_game_state(self):
        # TODO(5): функция должна возвращать состояние игры:
        #  победа, поражение или продолжение игры.
        #  Найдена открытая клетка с миной - поражение.
        #  Найдена закрытая клетка без мины - продолжение.
        #  Иначе победа.
        return GameState.progress

    def flag_mines(self):
        for row in self.field.cells:
            for cell in row:
                if cell.has_mine:
                    cell.has_flag = True

    def open_mines(self):
        for row in self.field.cells:
            for cell in row:
                if cell.has_mine and not cell.has_flag:
                    cell.is_open = True

    def draw(self):
        self.screen.fill(self.menu_color)
        self.draw_menu()
        self.draw_field()
        # TODO(6): вызовы методов draw_mines и draw_mine_counts
        #  нужны только для отладки. После завершения работы
        #  над приложением их нужно убрать.
        self.draw_mines()
        self.draw_mine_counts()

    def draw_menu(self):
        flags_set = self.count_flags()
        flags_left = self.field.mine_count - flags_set
        flag_counter = self.menu_font.render(
            str(flags_left), True, self.menu_font_color)

        self.draw_flag(self.screen_width - 110, 15)
        self.screen.blit(flag_counter, (self.screen_width - 70, 20))

        if self.game_state == GameState.progress:
            return

        text = 'You win!' if self.game_state == GameState.win else 'You lose'
        rendered_text = self.menu_font.render(text, True, self.menu_font_color)
        self.screen.blit(rendered_text, (20, 20))

    def count_flags(self):
        flags_count = 0
        for row in self.field.cells:
            for cell in row:
                if cell.has_flag:
                    flags_count += 1
        return flags_count

    def draw_field(self):
        for cell_row in range(len(self.field.cells)):
            for cell_col in range(len(self.field.cells[cell_row])):
                self.draw_cell(cell_row, cell_col)
        self.hover_cell()

    def draw_cell(self, cell_row, cell_col, hovered=False):
        cell_x, cell_y = self.get_cell_pos(cell_row, cell_col)
        cell = self.field.cells[cell_row][cell_col]
        rect = pygame.Rect(cell_x, cell_y, self.cell_side, self.cell_side)
        if cell.is_open:
            color_type = self.get_cell_color_type(cell_row, cell_col)
            color = self.open_colors[color_type]
            pygame.draw.rect(self.screen, color, rect)
            if cell.has_mine:
                pygame.draw.rect(self.screen, self.mined_cell_color, rect)
                self.draw_mine(cell_row, cell_col)
            else:
                self.draw_mine_count(cell_row, cell_col)
        else:
            if hovered:
                color_type = self.get_cell_color_type(cell_row, cell_col)
                color = self.selected_colors[color_type]
            else:
                color_type = self.get_cell_color_type(cell_row, cell_col)
                color = self.closed_colors[color_type]
            pygame.draw.rect(self.screen, color, rect)
            if cell.has_flag:
                if self.game_state == GameState.lose and not cell.has_mine:
                    self.draw_cross(cell_x, cell_y)
                else:
                    self.draw_flag(cell_x, cell_y)

    def draw_cross(self, x, y):
        pygame.draw.line(
            self.screen, self.flag_color,
            (x, y), (x + self.cell_side, y + self.cell_side),
            self.cross_line_width)
        pygame.draw.line(
            self.screen, self.flag_color,
            (x + self.cell_side, y), (x, y + self.cell_side),
            self.cross_line_width)

    def draw_flag(self, x, y):
        points = (
            (x + 10, y + 6),
            (x + self.cell_side - 6, y + self.cell_side // 2),
            (x + 10, y + self.cell_side - 6)
        )
        pygame.gfxdraw.aapolygon(self.screen, points, self.flag_color)
        pygame.gfxdraw.filled_polygon(self.screen, points, self.flag_color)

    def get_cell_color_type(self, cell_row, cell_col):
        return (
            (cell_row * self.field.width + cell_col + cell_row)
            % len(self.closed_colors))

    def hover_cell(self):
        cursor_x, cursor_y = pygame.mouse.get_pos()
        if cursor_y < self.menu_height:
            return

        cell_row, cell_col = self.get_cell_by_pos(cursor_x, cursor_y)
        self.draw_cell(cell_row, cell_col, hovered=True)
        for cell_row, cell_col in self.highlighted_cells:
            cell = self.field.cells[cell_row][cell_col]
            if not cell.has_flag:
                self.draw_cell(cell_row, cell_col, hovered=True)

    def get_cell_by_pos(self, cursor_x, cursor_y):
        """
        :return: (row, col)
        """
        return (
            (cursor_y - self.menu_height) // self.cell_side,
            cursor_x // self.cell_side)

    def cursor_in_field(self, cursor_x, cursor_y):
        return (0 <= cursor_x <= self.screen_width
                and self.menu_height <= cursor_y <= self.screen_height)

    def draw_mines(self):
        for cell_row in range(len(self.field.cells)):
            for cell_col in range(len(self.field.cells[cell_row])):
                if self.field.cells[cell_row][cell_col].has_mine:
                    self.draw_mine(cell_row, cell_col)

    def draw_mine(self, cell_row, cell_col):
        cell_x, cell_y = self.get_cell_pos(cell_row, cell_col)
        rect = pygame.Rect(cell_x, cell_y, self.cell_side, self.cell_side)
        self.screen.blit(self.mine_img, rect)

    def draw_mine_counts(self):
        for cell_row in range(len(self.field.cells)):
            for cell_col in range(len(self.field.cells[cell_row])):
                if not self.field.cells[cell_row][cell_col].has_mine:
                    self.draw_mine_count(cell_row, cell_col)

    def draw_mine_count(self, cell_row, cell_col):
        cell = self.field.cells[cell_row][cell_col]
        if cell.near_mine_count == 0:
            return
        color = self.get_mine_count_color(cell.near_mine_count)
        text = self.menu_font.render(
            str(cell.near_mine_count), True, color)
        cell_x, cell_y = self.get_cell_pos(cell_row, cell_col)
        self.screen.blit(text, (cell_x + 8, cell_y + 5))

    def get_cell_pos(self, cell_row, cell_col):
        cell_x = cell_col * self.cell_side
        cell_y = self.menu_height + cell_row * self.cell_side
        return cell_x, cell_y

    def get_mine_count_color(self, near_mine_count):
        color_number = (near_mine_count - 1) % len(self.mine_count_colors)
        return self.mine_count_colors[color_number]


class Field:
    def __init__(self, width, height, mine_count):
        self.cells = []
        self.mine_count = mine_count
        self.width = width
        self.height = height

        for row in range(height):
            cells_row = []
            for col in range(width):
                cells_row.append(Cell())
            self.cells.append(cells_row)

        self.place_mines()
        self.set_mine_count()

    def toggle_flag(self, cell_row, cell_col):
        # TODO(1): если клетка закрыта, то установить или снять с нее флаг.
        #  Нужные поля: cell.is_open, cell.has_flag
        cell = self.cells[cell_row][cell_col]
        if not cell.is_open:
            cell.has_flag = not cell.has_flag

    def place_mines(self):
        # TODO(2): реализовать расстановку мин в случайных клетках.
        #  Общее количество мин - self.mine_count
        #  Сначала предложите оптимальный алгоритм!
        mined_cells = list(range(self.width * self.height))
        random.shuffle(mined_cells)
        for cell_number in mined_cells[:self.mine_count]:
            cell_row = cell_number // self.width
            cell_coll = cell_number % self.width
            self.cells[cell_row][cell_coll].has_mine = True

    def set_mine_count(self):
        # TODO(3.1): для каждой клетки определить количество мин
        #  в соседних позициях.
        #  * Для получения соседних клеток воспользуйтесь методом
        #    self.get_near_cells(cell_row, cell_col).
        #  * Для вычисления количества мин нужно реализовать метод
        #    self.count_mines(near_cells).
        ...

    def count_mines(self, cells):
        # TODO(3.2): реализовать подсчет количества мин в клетках cells.
        #  Если в клетке есть мина, то ее атрибут has_mine равен True.
        ...

    def open_cell(self, cell_row, cell_col):
        cell = self.cells[cell_row][cell_col]

        # TODO(4): реализовать обработку открытия клетки.
        # TODO(4.1): если клетка уже открыта или отмечена флагом,
        #  то не делать ничего, выйти из функции.
        # TODO(4.2): отметить клетку как открытую.
        # TODO(4.3): если в клетке расположена мина, выйти из функции.
        # TODO(4.4): Если вокруг клетки есть мины, открыть клетку.
        #   Иначе получить координаты соседей с помощью метода
        #   self.get_near_cells_coords и для каждого соседа
        #   повторить этот алгоритм открытия клетки.
        ...

    def open_near(self, cell_row, cell_col):
        cell = self.cells[cell_row][cell_col]
        # TODO(7): реализовать открытие соседних клеток.
        # TODO(7.1): если клетка открыта, не делать ничего.
        # TODO(7.2): если вокруг клетки нет мин, не делать ничего.
        # TODO(7.3): вычислить количество флагов вокруг клетки с помощью
        #  метода self.count_near_flags
        # TODO(7.5): если количество флагов не равно количеству
        #  мин вокруг ячейки, не делать ничего.
        # TODO(7.6): использовать алгоритм открытия ячейки для всех соседей.
        #  Поможет метод self.get_near_cells_coords.
        ...

    def count_near_flags(self, cell_row, cell_col):
        # TODO(7.3): вычислить количество флагов, установленных вокруг клетки.
        #  * Получить координаты клеток с помощью метода
        #    self.get_near_cells_coords
        #  * Обойти ячейки, посчитать количество флагов.
        ...

    def get_near_cells(self, cell_row, cell_col):
        cells_coords = self.get_near_cells_coords(cell_row, cell_col)
        cells = []
        for cell_row, cell_col in cells_coords:
            cells.append(self.cells[cell_row][cell_col])
        return cells

    def get_near_cells_coords(self, cell_row, cell_col):
        cells_coords = (
            self.maybe_get_cell(cell_row - 1, cell_col - 1),
            self.maybe_get_cell(cell_row - 1, cell_col),
            self.maybe_get_cell(cell_row - 1, cell_col + 1),
            self.maybe_get_cell(cell_row, cell_col + 1),
            self.maybe_get_cell(cell_row + 1, cell_col + 1),
            self.maybe_get_cell(cell_row + 1, cell_col),
            self.maybe_get_cell(cell_row + 1, cell_col - 1),
            self.maybe_get_cell(cell_row, cell_col - 1)
        )
        return list(filter(None, cells_coords))

    def maybe_get_cell(self, cell_row, cell_col):
        if (cell_row < 0 or cell_row >= len(self.cells)
                or cell_col < 0 or cell_col >= len(self.cells[0])):
            return None
        return (cell_row, cell_col)


class Cell:
    def __init__(self):
        self.is_open = False
        self.has_flag = False
        self.has_mine = False
        self.near_mine_count = 0


pygame.init()
pygame.display.set_caption("Mines")
ui = Ui()
ui.start_level(NORMAL)
ui.handle_events()
