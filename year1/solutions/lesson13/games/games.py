import easygui
import random


def rock_paper_scissors():
    choices = [
        'rock.png',
        'paper.png',
        'scissors.png'
    ]
    while True:
        choice = easygui.buttonbox('Твой ход!', choices=[], images=choices)
        if choice is None:
            break
        cpu_choice = random.choice(choices)
        rel = compare(choice, cpu_choice)
        easygui.buttonbox('Результат', choices=[], images=[choice, rel, cpu_choice])


def compare(left, right):
    if beats(left, right):
        return 'gt.png'

    if beats(right, left):
        return 'lt.png'

    return 'eq.png'


def beats(a, b):
    if a.startswith('rock') and b.startswith('scissors'):
        return True

    if a.startswith('paper') and b.startswith('rock'):
        return True

    if a.startswith('scissors') and b.startswith('paper'):
        return True

    return False


def guess_the_number():
    lower = 0
    upper = 200
    secret_number = random.randint(lower, upper)
    msg = f'Угадай число от {lower} до {upper}'
    while True:
        number = easygui.integerbox(msg, lowerbound=lower, upperbound=upper)

        if number is None:
            easygui.msgbox(f'Сдался? Это было {secret_number}')
            break

        number = int(number)
        
        if number == secret_number:
            easygui.msgbox(f'Да, это {number}')
            break

        if number > secret_number:
            msg = f'Мое число меньше, чем {number}'
        else:
            msg = f'Мое число больше, чем {number}'


games = [
    'Камень-ножницы-бумага',
    'Угадай число'
]

games_entry_points = [
    rock_paper_scissors,
    guess_the_number
]

while True:
    res = easygui.buttonbox('Выбери игру!', choices=games)
    if res is None:
        break
    games_entry_points[games.index(res)]()
