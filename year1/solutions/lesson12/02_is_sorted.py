def is_sorted(lst):
    if len(lst) == 0:
        return True
    prev = lst[0]
    for item in lst:
        if item < prev:
            return False
        prev = item

    return True


assert is_sorted([]) == True
assert is_sorted([42]) == True
assert is_sorted([3, 14, 15, 92]) == True
assert is_sorted([1, 1, 1]) == True
assert is_sorted([1, 1, 2, 2, 3, 3, 3]) == True
assert is_sorted([2, 1]) == False
assert is_sorted([1, 2, 1]) == False
