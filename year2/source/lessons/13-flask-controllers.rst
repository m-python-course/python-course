.. _FlaskControllers:

Flask. Обработчики запросов.
=============================

Общее
-----
В предыдущем разделе было реализовано web-приложение с использованием Flask, которое может обрабатывать GET-запросы по корневому пути.

.. code::

  @app.route('/')
  def sample():
      return 'Был получен GET-запрос.'


Обработчик - обычная функция, которая отмечается как обработчик с указанием пути: ``@app.route('/')``.


HTTP-методы
------------
При реализации простого :ref:`сервера <SimpleServer>` требовалось реализовывать специальные методы для обработки различных HTTP-методов.

 * do_GET
 * do_POST
 * и т.д.

При работе с Flask достаточно передать список методов, которые должен принимать тот или иной обработчик:

.. code::

  @app.route('/', methods=['GET'])
  def sample():
      return 'Был получен GET-запрос.'


Таким образом, обработчик ``sample`` будет обрабатывать исключительно GET-запросы. При попытке обратиться по указанному пути
с другим HTTP-методом будет вызвана ошибка.


Обработка параметров
--------------------
Любое приложение работает с данными. Данные в приложение могут поступать несколькими способами:

* В пути запроса(GET)
* В теле запроса(POST, PUT и т.д.)

Обработка параметров в пути запросов
-------------------------------------
Для запроса с GET-методом возможно передать данные только в пути самого запроса, и Flask предоставляет удобный способ для передачи
их в обработчик:

.. code::

  @app.route('/say/<phrase>')
  def say(phrase):
      return f'Была получена фраза {phrase}'


Для передачи параметра в обработчик достаточоно добавить его имя в путь ``@app.route('/say/<phrase>')``. Важно, что бы оно начиналось
со знака ``<``, и заканчивалось ``>``. Затем это же имя указывается в качестве аргумента функции-обработчика.
Flask так же предоставляет возможность указать и тип принимаемого параметра:

.. code::

  @app.route('/sum/<int:number>')
  def sum_numbers(number):
      return f'Результат сложения 5 и {number} = {5+number}'


Для указания типа достаточно добавить его название перед именем параметра: ``<int:number>``.


Обработка параметров в теле запросов
-------------------------------------
Как было видно из предыдущих пунктов, в обрабочик не передается сам запрос. Для того что бы получить к нему доступ требуется
импортировать специальный объект Flask - ``request``, в нем то и будет содержаться вся информация по запросу:

.. code::

  from flask import request

  @app.route('/calc/', methods=['GET', 'POST'])
  def calc():
      if request.method == 'POST':
          a = int(request.form['a'])
          b = int(request.form['b'])
          result = a + b
          return f'{a} + {b} = {result}'
      return f'Был получен {request.method} запрос.'


Задания
-------
Разработайте приложение по просмотру и добавлению информации об учениках курса.
Функции приложения:

#. Просмотр списка учащихся(ФИО, курс).
#. Просмотр полной информации о конкретном учащемся.
#. Добавление нового учащегося.

Информация об учащемся должна содержать: ФИО, курс, сколько лет.
Для хранения данных используйте словари(``dict``).


Дополнительно
--------------
Доработайте приложение так, что бы для хранения информации использовались `файлы <https://python-course.readthedocs.io/projects/year1/en/latest/lessons/14-files.html>`_.

