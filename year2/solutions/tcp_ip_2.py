'''1 уровень, функция network_interface - отвечает за соединение с сетью
 передаём в функцию 2 аргумента: 
 1 аргумент, data - это данные в виде бинарного числа
 2 аргумент, connection - это тип соединения (1 - 'Ethernet', 2 -'Wi-Fi', другие числа - "No connection")
 возвращаем:
 - если удалось установить соединение - то кортеж из 2 значений: данные в виде строки из нулей и единиц и тип соединения
 - если не удалось установить соединение, то строку "No connection"'''

def network_interface(data, connection):
    #преобразуем бинарные данные в строку, чтобы потом было проще работать
    data_decoded = str (bin(data))[2:]
    if connection == 1:
        return data_decoded, 'Ethernet'
    elif connection == 2:
        return data_decoded, 'Wi-Fi'
    else:
        return "No connection"
    
''' 2 уровень, функция internet() - отвечает за маршрутизацию в сети. Она ищет кому и как доставить данные
 передаём в функцию результат работы функции  network_interface()
 возвращаем данные в виде строки, если данные переданы в кортеже и адресованы нам, иначе - передаём None'''
    
def internet(data):
    #ищем данные в нужном формате (кортеж)
    if type(data) == tuple:
        # проверяем, для нас ли эти данные, если для нас - то возвращаем строку с данными
        if data[0][0] == '1':
            return data[0][1:]
    return None
        
''' 3 уровень, функция transport() - отвечает за способ передачи данных и их транспортировку
 передаём в функцию результат работы функции internet()
 возвращаем:
  - если c предыдущего уровня получены данные, то формируем кортеж из 2 значений: данные, протокол передачи
  - None - если данные не получены'''                
        
def transport(data):
    if data != None:
        if data[1] == '1':
            protocol = 'TCP'
        elif data[1] == '0':
            protocol = 'UDP'
        else:
            protocol = 'Unknown'
        return data[1:], protocol
    return None
''' 4 уровень, функция application() - отвечает за формат данных, их расшифровку
передаём в функцию результат работы функции transport()
возвращаем:  '''

def application (data):
    if data[1] != 'Unknown':
        if data[0][-2:] == '01':
            protocol = 'SMTP'
            mes = 'Вы получили письмо'
        elif data[0][-2:] == '10':
            protocol = 'HTTP'
            mes = 'Вы открыли страничку с котиками'
        elif data[0][-2:] == '11':
            protocol = 'FTP'
            mes = 'Вы получили файл'
        else:
            protocol = 'Unknown'
            mes = 'Протокол не указан, данные расшифровать невозможно'
        return data[0][:-2], protocol, mes
    return "Данные были потеряны"
    
data = 0b110110111
print (bin(data))
data_from_network = network_interface (data,1)
print ('network_interface level - ', data_from_network)
data_ip = internet (data_from_network)
print ('internet level - ', data_ip)
data_transport = transport (data_ip)
print ('transport level - ', data_transport)
data_app = application(data_transport)
print ('application level - ', data_app)
