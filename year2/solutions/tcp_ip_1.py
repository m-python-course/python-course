''' 4 уровень, функция application() - отвечает за формат данных, их шифрование
input функции - данные, которые хотим отправить по сети, протокол данных ('SMTP','HTTP','FTP') 
output: данные + информация про формат данных и шифрование '''

def en_application (data, protocol):
    if protocol == 'SMTP':
        data = data + '01'
    elif protocol == 'HTTP':
        data = data + '10'
    elif protocol == 'FTP':
        data = data + '11'
    return data

''' 3 уровень, функция transport() - отвечает за способ передачи данных и их транспортировку
 input: результат работы функции application(), протокол транспортировки данных ('TCP', 'UDP') 
 output: данные + информация про транспортировку'''                
        
def en_transport(data, protocol):
    if data != None:
        if protocol == 'TCP':
            data = '1' + data
        elif protocol == 'UDP':
            data = '0' + data
    return data
    
''' 2 уровень, функция internet() - отвечает за маршрутизацию в сети. Она ищет кому и как доставить данные
 input: результат работы функции transport()
 output: данные + информация про адресата и маршрут доставки'''
    
def en_internet(data):
    data = '1' + data
    return data

'''1 уровень, функция network_interface - отвечает за соединение с сетью
 input: результат работы функции internet(), информация о соединении
 output: кортеж из 2 значений: 
 1. данные, преобразованные в бинарный формат
 2. информация о типе соединения (1 - 'Ethernet', 2 -'Wi-Fi', другие числа - "No connection")'''

def en_network_interface(data, connection):
    data = bin(int(('0b'+ data), 2))
    if connection == 'Ethernet':
        return data, 1
    elif connection == 'Wi-Fi':
        return data, 2
    else:
        return data, 0

           
data = '01101'
print (data)
data_app = en_application(data,'SMTP')
print ('application level - ', data_app)
data_transport = en_transport (data_app,'TCP')
print ('transport level - ', data_transport)
data_ip = en_internet (data_transport)
print ('internet level - ', data_ip)
data_from_network = en_network_interface (data_ip,'Wi-Fi')
print ('network_interface level - ', data_from_network)
