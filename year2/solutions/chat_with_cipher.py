import socket
import struct
import sys

def encode_symbol(first, last, key, symbol):
    encoded_symbol = first + (ord(symbol)-first+key)%(last-first+1)
    return chr(encoded_symbol)
def encode_string(string, key):
    res = ""
    for c in string:
        if c.islower() and c.isalpha():
            res += encode_symbol(ord('a'), ord('z'), key, c)
        elif c.isupper() and c.isalpha():
            res += encode_symbol(ord('A'), ord('Z'), key, c)
        elif c.isnumeric():
            res += encode_symbol(ord('0'), ord('9'), key, c)
        else:
            res += c
    return res

def recieve():
    host = "localhost"
    port = int(input("Введите порт: "))
    key  = int(input("Ключ шифрования: "))
    server_address = (host, port)
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(server_address)

    server_socket.listen(1)
    conn, client_address = server_socket.accept()

    total_size, payload = receive_message(conn)
    msg = decode_message(total_size, payload)
    encoded = encode_string(msg, -key)
    
    print(f"Полученно сообщение {encoded}")

    conn.close()
    server_socket.close()

def receive_message(receiver):
    total_size, = struct.unpack("<I", receiver.recv(4))
    print(f"total-size {total_size}")
    total_recieved = 0
    payload = b""
    while total_recieved < total_size:
        block = receiver.recv(total_size - total_recieved)
        total_recieved += len(block)
        payload += block
        print(f"Получено {total_recieved}/{total_size}")
    return total_size, payload

def decode_message(total_size, payload):
    offset = 0
    length_field_size = 4
    message_size = total_size - length_field_size
    content, = struct.unpack_from(f"<{message_size}s", payload, offset)
    print(f"Content:  {content}")
    return content.decode("utf8")

def send():
    host = input("Введите имя сервера: ")
    if not host:
        host = 'localhost'
    port = int(input("Введите порт: "))
    key = int(input("Ключ шфирования: "))
    server_address = (host, port)

    path = input("Путь к файлу: ")
    f = open(path, "rb")
    msg = f.read()
    # msg = encode_string(msg, key)
    
    with socket.create_connection(server_address) as sock:
        message = encode_message(msg) 
        send_message(sock, message)
    f.close()

def send_message(sock, message):
    total_size = len(message)
    total_sent = 0
    while total_sent < total_size:
        sent = sock.send(message[total_sent:])
        total_sent += sent
        print(f"Отправлено {total_sent}/{total_size}")

def encode_message(content):
    length_field_size = 4
    content = content.encode('utf-8')
    content_size = len(content)
    message_size = length_field_size + content_size
    return struct.pack(f"<I{message_size}s", message_size, content)

def main():
    print("1 - Отправить сообщение")
    print("2 - Получить сообщение")
    mode = int(input("Выберите режим работы: "))
    if mode == 1:
        send()
    elif mode == 2:
        recieve()
    input("Нажмите Enter...")

if __name__ == "__main__":
    sys.exit(main())
